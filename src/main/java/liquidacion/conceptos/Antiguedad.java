
package liquidacion.conceptos;

import java.math.BigDecimal;
import liquidacion.legajo.Empleado;

public class Antiguedad implements ConceptoHaber{

    public BigDecimal calculo(Empleado empleado) {
        BigDecimal resultado=new BigDecimal(600.00);
        resultado=resultado.multiply(new BigDecimal(empleado.antiguedad()));
        return resultado;
    }
    
}