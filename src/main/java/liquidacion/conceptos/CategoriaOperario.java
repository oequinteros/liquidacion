package liquidacion.conceptos;

import java.math.BigDecimal;
import liquidacion.legajo.Empleado;
import liquidacion.legajo.Operario;
import liquidacion.legajo.TURNO;

public class CategoriaOperario implements ConceptoHaber{

    public BigDecimal calculo(Empleado empleado) {
        BigDecimal resultado=new BigDecimal(1000.20);
        Operario op = (Operario)empleado;
        if (op.getTurno().equals(TURNO.NOCTURNO)){
            resultado=resultado.add(new BigDecimal(500.50));
        }
        return resultado;
    }
    
}
