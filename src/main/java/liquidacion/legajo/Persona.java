
package liquidacion.legajo;

public abstract class Persona {
    private Integer dni;
    private String apellido;
    private String nombre;

    public Persona(Integer dni,String apellido,String nombre){
        this.dni=dni;
        this.apellido=apellido.toUpperCase();
        this.nombre=nombre.toUpperCase();
    }
    public Integer getDni() {
        return dni;
    }

    public void setDni(Integer dni) {
        this.dni = dni;
    }

    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido.toUpperCase();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
