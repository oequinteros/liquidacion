
package liquidacion.legajo;

import java.time.LocalDate;

public class Operario extends Empleado{
    private TURNO turno;

    public Operario(Integer dni, String apellido, String nombre, Integer nroLegajo,TURNO turno,LocalDate fechaIngreso) {
        super(dni, apellido, nombre, nroLegajo,fechaIngreso);
        this.turno=turno;        
    }

    public TURNO getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = TURNO.valueOf(turno);
    }
}
