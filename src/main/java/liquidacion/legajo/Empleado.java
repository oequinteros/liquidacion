package liquidacion.legajo;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.Period;
import java.math.BigDecimal;
import java.util.ArrayList;
import liquidacion.conceptos.ConceptoHaber;

public abstract class Empleado extends Persona{
    private Integer nroLegajo;
    private LocalDate fechaIngreso;
    private ArrayList<ConceptoHaber> haberes= new ArrayList<ConceptoHaber>();
    public Empleado(Integer dni,String apellido,String nombre,Integer nroLegajo, LocalDate fechaIngreso){
        super(dni,apellido,nombre);
        this.nroLegajo=nroLegajo;
        this.fechaIngreso=fechaIngreso;
    }
    public Empleado(Integer dni,String apellido,String nombre){
       super(dni,apellido,nombre); 
    }
    public Integer getNroLegajo() {
        return nroLegajo;
    }
    public void setNroLegajo(Integer nroLegajo) {
        this.nroLegajo = nroLegajo;
    }
    public void addHaberes(ConceptoHaber haber){
        this.haberes.add(haber);
    }
    public Integer antiguedad(){
        Period intervaloPeriodo = Period.between(fechaIngreso, LocalDate.now());
        return intervaloPeriodo.getYears();
    }
    public BigDecimal liquidarHaberes(){
        BigDecimal resultado = new BigDecimal(0.00);
        for (ConceptoHaber haber : haberes) {
            resultado = resultado.add(haber.calculo(this));
        }
        return resultado.setScale(2, RoundingMode.HALF_EVEN);
    }
    
}
