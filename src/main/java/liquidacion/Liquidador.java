package liquidacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import liquidacion.legajo.Empleado;

public class Liquidador {
    private ArrayList<Empleado> empleados = new ArrayList<Empleado>();
    
    public void agregarEmpleado(Empleado empleado){
        this.empleados.add(empleado);
    }
    public BigDecimal liquidar(){
        BigDecimal sumatoria=new BigDecimal(0.00);
        for (Empleado empleado : empleados) {
            sumatoria=sumatoria.add(empleado.liquidarHaberes());
        }
        return sumatoria;
    }
}
